/***************************************************
 ** @Desc : This file for 记录实现方法
 ** @Time : 2019.05.23 16:05
 ** @Author : Joker
 ** @File : record_impl
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.05.23 16:05
 ** @Software: GoLand
****************************************************/
package implement

import (
	"github.com/tealeg/xlsx"
	"os"
	"recharge/models"
	"recharge/sys"
	"recharge/utils"
	"strings"
	"time"
)

type RecordImpl struct{}

// 删除指定目录下的所有文件
func (*RecordImpl) RemoveFileOrMKDir(path string) {
	defer func() {
		if r := recover(); r != nil {
			sys.LogEmergency("并发竞争，操作了不存在的目录！现已恢复现场！")
			time.Sleep(1 * time.Second)
		}
	}()

	_, err := os.Stat(path)
	if err != nil {
		if os.IsExist(err) {
			os.RemoveAll(path)
			os.Mkdir(path, os.ModePerm)
		} else {
			os.Mkdir(path, os.ModePerm)
		}
	} else {
		os.RemoveAll(path)
		os.Mkdir(path, os.ModePerm)
	}
}

func (the *RecordImpl) CreateXLSXFileForRecord(listT []models.TransferRecord, listP []models.WithdrawRecord, listR []models.RechargeRecord) (string, int) {
	var (
		path     = "static/excel/record/"
		fileName = "template" + globalMethod.GetNowTimeV2() + globalMethod.RandomString(6)
		fileType = ".xlsx"
	)

	file := xlsx.NewFile()

	// 写入代付记录
	sheetP, err := file.AddSheet("代付记录")
	if err != nil {
		return err.Error(), utils.FailedFlag
	}

	// 第一行
	row := sheetP.AddRow()
	row.SetHeightCM(1)
	cell := row.AddCell()
	cell.Value = "流水号"
	cell = row.AddCell()
	cell.Value = "提现银行卡号"
	cell = row.AddCell()
	cell.Value = "银行户名"
	cell = row.AddCell()
	cell.Value = "类型"
	cell = row.AddCell()
	cell.Value = "更新时间"
	cell = row.AddCell()
	cell.Value = "订单编号"
	cell = row.AddCell()
	cell.Value = "提现金额"
	for _, v := range listP {
		addRow := sheetP.AddRow()
		addCell := addRow.AddCell()
		addCell.Value = v.SerialNumber
		addCell = addRow.AddCell()
		addCell.Value = v.WhAccountNo
		addCell = addRow.AddCell()
		addCell.Value = v.WhAccountName

		userType := ""
		if strings.Compare("1", v.WhUserType) == 0 {
			userType = "对私"
		} else {
			userType = "对公"
		}
		addCell = addRow.AddCell()
		addCell.Value = userType

		addCell = addRow.AddCell()
		addCell.Value = v.EditTime
		addCell = addRow.AddCell()
		addCell.Value = v.WhOrderId
		addCell = addRow.AddCell()
		addCell.SetFloat(v.WhAmount)
	}

	// 写入充值记录
	sheetR, err := file.AddSheet("线下充值记录")
	if err != nil {
		return err.Error(), utils.FailedFlag
	}

	// 第一行
	row = sheetR.AddRow()
	row.SetHeightCM(1)
	cell = row.AddCell()
	cell.Value = "流水号"
	cell = row.AddCell()
	cell.Value = "充值银行卡号"
	cell = row.AddCell()
	cell.Value = "银行户名"
	cell = row.AddCell()
	cell.Value = "类型"
	cell = row.AddCell()
	cell.Value = "更新时间"
	cell = row.AddCell()
	cell.Value = "订单编号"
	cell = row.AddCell()
	cell.Value = "充值金额"
	for _, v := range listR {
		addRow := sheetR.AddRow()
		addCell := addRow.AddCell()
		addCell.Value = v.SerialNumber
		addCell = addRow.AddCell()
		addCell.Value = v.ReAccountNo
		addCell = addRow.AddCell()
		addCell.Value = v.ReAccountName

		userType := ""
		if strings.Compare("NUCC", v.ReRecevieBank) == 0 {
			userType = "网联"
		}
		addCell = addRow.AddCell()
		addCell.Value = userType

		addCell = addRow.AddCell()
		addCell.Value = v.EditTime
		addCell = addRow.AddCell()
		addCell.Value = v.ReOrderId
		addCell = addRow.AddCell()
		addCell.SetFloat(v.ReAmount)
	}

	// 写入B2C充值记录
	sheetT, err := file.AddSheet("B2C充值记录")
	if err != nil {
		return err.Error(), utils.FAILED_FLAG
	}

	// 第一行
	row = sheetT.AddRow()
	row.SetHeightCM(1)
	cell = row.AddCell()
	cell.Value = "流水号"
	cell = row.AddCell()
	cell.Value = "更新时间"
	cell = row.AddCell()
	cell.Value = "订单编号"
	cell = row.AddCell()
	cell.Value = "充值金额"
	for _, v := range listT {
		addRow := sheetT.AddRow()
		addCell := addRow.AddCell()
		addCell.Value = v.SerialNumber

		addCell = addRow.AddCell()
		addCell.Value = v.EditTime
		addCell = addRow.AddCell()
		addCell.Value = v.TrOrderId
		addCell = addRow.AddCell()
		addCell.SetFloat(v.TrAmount)
	}

	// 保存文件
	s := path + fileName + fileType
	err = file.Save(s)
	if err != nil {
		return err.Error(), utils.FAILED_FLAG
	}
	return fileName + fileType, utils.FAILED_FLAG
}

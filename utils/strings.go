/***************************************************
 ** @Desc : This file for 字符串常量
 ** @Time : 2019.04.01 11:45
 ** @Author : Joker
 ** @File : strings
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.01 11:45
 ** @Software: GoLand
****************************************************/
package utils

//短信配置
const (
	APIKEY     = "fd264aa****"
	TPL1       = 2512750
	URLSENDSMS = "https://sms.yunpian.com/v2/sms/tpl_single_send.json"
)

// 提现成功接收短信通知
const (
	TPL2   = 2453286
	MOBILE = "18670135299"
)

//成功与否的标记
const (
	FAILED_FLAG    = -9
	FAILED_STRING  = "操作失败! "
	SUCCESS_FLAG   = 9
	SUCCESS_STRING = "操作成功! "
)

//通道类型
const (
	XF = "XF"
)

var channelType = map[string]string{
	XF: "先锋通道",
}

func GetChannelType() map[string]string {
	return channelType
}

//用户登录默认密码
const UPASSWORD = "tcp@143258"

//秘钥头尾
const (
	STARTKEY = "\n-----BEGIN PUBLIC KEY-----\n"
	ENDKEY   = "\n-----END PUBLIC KEY-----"
)

// 订单状态
const (
	I = "I"
	S = "S"
	F = "F"
)

var orderStatus = map[string]string{
	S: "成功",
	I: "处理中",
	F: "失败",
}

func GetOrderStatus() map[string]string {
	return orderStatus
}

var apiOrderStatus = map[string]string{
	S: "SUCCESS",
	I: "WAITING_PAYMENT",
	F: "FAILED",
}

func GetApiOrderStatus() map[string]string {
	return apiOrderStatus
}

// 订单类别
const (
	A = "a" //对接
	P = "p" //平台
)

const (
	SingleMaxForPay      = 45000
	SingleMaxForTransfer = 20000
	SingleMinForRecharge = 20
)

/***************************************************
 ** @Desc : This file for 用户信息扩展数据模型
 ** @Time : 2019.04.16 14:10 
 ** @Author : Joker
 ** @File : user_info_expansion
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.16 14:10
 ** @Software: GoLand
****************************************************/
package models

import (
	"github.com/astaxie/beego/orm"
	"recharge/sys"
	"recharge/utils"
)

type UserInfoExpansion struct {
	UserId     int `orm:"pk;column(user_id)"`
	CreateTime string
	EditTime   string
	Version    int
	Ips        string `orm:"type(text)"`
}

func (*UserInfoExpansion) TableEngine() string {
	return "INNODB"
}

func (*UserInfoExpansion) TableName() string {
	return UserInfoExpansionTBName()
}

/* *
 * @Description: 添加用户扩展信息
 * @Author: Joker
 * @Date: 2019-4-15 17:53:58
 * @Param: sr: 用户扩展信息
 * @return: int: 判断成功的标识
 * @return: int64:	在表中哪一行插入
 **/
func (*UserInfoExpansion) InsertUserInfoExpansion(sr UserInfoExpansion) (int, int64) {
	om := orm.NewOrm()
	in, _ := om.QueryTable(UserInfoExpansionTBName()).PrepareInsert()
	id, err := in.Insert(&sr)
	if err != nil {
		sys.LogError("InsertUserHistory failed to insert for: ", err)
		return utils.FAILED_FLAG, id
	}
	return utils.SUCCESS_FLAG, id
}

//读取单个用户信息通过登录账号
func (*UserInfoExpansion) SelectOneUserInfoExpansion(id int) (userExpand UserInfoExpansion) {
	om := orm.NewOrm()
	err := om.QueryTable(UserInfoExpansionTBName()).Filter("user_id", id).One(&userExpand)
	if err != nil {
		sys.LogError("SelectOneUserInfoExpansion failed to select user: ", err)
	}
	return userExpand
}

// 修改用户扩展信息
func (*UserInfoExpansion) UpdateUserInfoExpansion(u UserInfoExpansion) int {
	om := orm.NewOrm()
	_, err := om.QueryTable(UserInfoExpansionTBName()).Filter("user_id", u.UserId).Update(orm.Params{
		"version":   orm.ColValue(orm.ColAdd, 1),
		"edit_time": u.EditTime,
		"ips":       u.Ips,
	})
	if err != nil {
		sys.LogError("UpdateUserInfoExpansion failed to update for:", err)
		return utils.FAILED_FLAG
	}
	return utils.SUCCESS_FLAG
}

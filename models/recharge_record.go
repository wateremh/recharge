/***************************************************
 ** @Desc : This file for 充值记录数据模型
 ** @Time : 2019.04.10 11:47 
 ** @Author : Joker
 ** @File : recharge_record
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.10 11:47
 ** @Software: GoLand
****************************************************/
package models

import (
	"github.com/astaxie/beego/orm"
	"recharge/sys"
	"recharge/utils"
)

type RechargeRecord struct {
	Id                int
	UserId            int
	Version           int
	MerchantNo        string
	CreateTime        string
	EditTime          string
	SerialNumber      string
	ReOrderId         string
	ReAmount          float64
	ReAccountNo       string
	ReAccountName     string
	ReCertificateType string
	ReCertificateNo   string
	ReMobileNo        string
	ReRecevieBank     string
	Status            string
	Remark            string
	RecordType        string
	NoticeUrl         string `orm:"type(text)"`
	RecordClass       string
	ApiNoticeUrl      string `orm:"type(text)"`
	Item              string

	UserName string `orm:"-"`
}

func (*RechargeRecord) TableEngine() string {
	return "INNODB"
}

func (*RechargeRecord) TableName() string {
	return RechargeRecordTBName()
}

/* *
 * @Description: 添加充值记录
 * @Author: Joker
 * @Date: 2019-4-10 11:53:28
 * @Param: Merchant
 * @return: int: 判断成功的标识
 * @return: int64:	在表中哪一行插入
 **/
func (*RechargeRecord) InsertRechargeRecord(r RechargeRecord) (int, int64) {
	om := orm.NewOrm()
	in, _ := om.QueryTable(RechargeRecordTBName()).PrepareInsert()
	id, err := in.Insert(&r)
	if err != nil {
		sys.LogError("InsertRechargeRecord failed to insert for: ", err)
		return utils.FAILED_FLAG, id
	}
	return utils.SUCCESS_FLAG, id
}

// 修改充值记录
func (*RechargeRecord) UpdateRechargeRecord(r RechargeRecord) int {
	om := orm.NewOrm()
	_, err := om.QueryTable(RechargeRecordTBName()).Filter("re_order_id", r.ReOrderId).Filter("merchant_no", r.MerchantNo).
		Update(orm.Params{
			"version":   orm.ColValue(orm.ColAdd, 1),
			"edit_time": r.EditTime,
			"status":    r.Status,
			"remark":    r.Remark,
		})
	if err != nil {
		sys.LogError("UpdateRechargeRecord failed to update for:", err)
		return utils.FAILED_FLAG
	}
	return utils.SUCCESS_FLAG
}

/*查询分页*/
func (*RechargeRecord) SelectRechargeRecordListPage(params map[string]interface{}, limit, offset int) (list []RechargeRecord) {
	om := orm.NewOrm()
	qt := om.QueryTable(RechargeRecordTBName())
	for k, v := range params {
		if v != "" {
			qt = qt.Filter(k, v)
		}
	}
	_, err := qt.OrderBy("-create_time").Limit(limit, offset).All(&list)
	if err != nil {
		sys.LogError("SelectRechargeRecordListPage failed to query paging for: ", err)
	}
	return list
}

func (*RechargeRecord) SelectRechargeRecordPageCount(params map[string]interface{}) int {
	om := orm.NewOrm()
	qt := om.QueryTable(RechargeRecordTBName())
	for k, v := range params {
		if v != "" {
			qt = qt.Filter(k, v)
		}
	}
	cnt, err := qt.Count()
	if err != nil {
		sys.LogError("SelectRechargeRecordPageCount failed to count for:", err)
	}
	return int(cnt)
}

// 读取单个代付记录
func (*RechargeRecord) SelectOneRechargeRecord(id string) (r RechargeRecord) {
	om := orm.NewOrm()
	_, err := om.QueryTable(RechargeRecordTBName()).Filter("id", id).All(&r)
	if err != nil {
		sys.LogError("SelectOneRechargeRecord failed to select one:", err)
	}
	return r
}

// 读取单个代付记录通过订单编号
func (*RechargeRecord) SelectOneRechargeRecordByOrderId(id string) (r RechargeRecord) {
	om := orm.NewOrm()
	_, err := om.QueryTable(RechargeRecordTBName()).Filter("re_order_id", id).All(&r)
	if err != nil {
		sys.LogError("SelectOneRechargeRecordByOrderId failed to select one:", err)
	}
	return r
}

//根据条件查询所有充值记录
func (*RechargeRecord) SelectAllRechargeRecordBy(params map[string]interface{}) (list []RechargeRecord) {
	om := orm.NewOrm()

	qt := om.QueryTable(RechargeRecordTBName())
	for k, v := range params {
		if v != "" {
			qt = qt.Filter(k, v)
		}
	}
	_, err := qt.OrderBy("-create_time").All(&list)
	if err != nil {
		sys.LogError("SelectAllRechargeRecordBy failed to query paging for: ", err)
	}

	return list
}

//判断订单是否存在
func (*RechargeRecord) RechargeRecordIsExit(orderId string) bool {
	om := orm.NewOrm()
	return om.QueryTable(RechargeRecordTBName()).Filter("re_order_id", orderId).Exist()
}

/***************************************************
 ** @Desc : This file for 首页js
 ** @Time : 2019.04.12 16:07
 ** @Author : Joker
 ** @File : index.js
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.12 16:07
 ** @Software: GoLand
 ****************************************************/

let index = {
    show_user_info: function () {
        $.ajax({
            type: "GET",
            url: "/merchant/count_all_recharge_pay/",
            cache: true,
            success: function (res) {
                if (res.code === 9) {
                    $("#today_recharge").html(res.todayRecharge);
                    $("#today_b2c_free").html(res.todayTransfer);
                    $("#today_recharge_all").html(res.recharge);
                    $("#today_free").html(res.pay);
                }
            },
            error: function (XMLHttpRequest) {
                toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
            }
        });
    },
    show_user_info_btn: function () {
        $.ajax({
            type: "GET",
            url: "/merchant/count_all_recharge_pay/",
            cache: true,
            success: function (res) {
                if (res.code === 9) {
                    swal("校对成功！", {
                        icon: "success",
                        closeOnClickOutside: false,
                    }).then(() => {
                        $("#today_recharge").html(res.todayRecharge);
                        $("#today_b2c_free").html(res.todayTransfer);
                        $("#today_recharge_all").html(res.recharge);
                        $("#today_free").html(res.pay);
                    });
                }
            },
            error: function (XMLHttpRequest) {
                toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
            }
        });
    },
    count_all_recharge_btn: function () {
        let start = $("#start2").val();
        let end = $("#end2").val();

        $.ajax({
            type: "POST",
            url: "/merchant/count_all_recharge_pay/",
            data: {
                start: start,
                end: end,
            },
            cache: true,
            success: function (res) {
                if (res.code === 9) {
                    swal("统计成功！", {
                        icon: "success",
                        closeOnClickOutside: false,
                    }).then(() => {
                        $("#all_line_recharge").html(res.todayRecharge);
                        $("#all_b2c_free").html(res.todayTransfer);
                        $("#all_today_recharge").html(res.recharge);
                        $("#all_today_free").html(res.pay);

                        $("#recharge_count").html(res.lenR);
                        $("#B2C_count").html(res.lenT);
                        $("#pay_count").html(res.lenP);
                        $("#all_fee").html(res.fee);
                    });
                }
            },
            error: function (XMLHttpRequest) {
                toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
            }
        });
    },
    make_excel_for_record: function () {
        let start = $("#start2").val();
        let end = $("#end2").val();

        if (start === "") {
            toastr.error("开始时间不能为空！");
        } else {
            $.ajax({
                type: "GET",
                url: "/merchant/make_excel/",
                data: {
                    start: start,
                    end: end,
                },
                cache: true,
                success: function (res) {
                    if (res.code === 9) {
                        let $form = $("<form method='get'></form>");
                        $form.attr("action", "/merchant/download_excel/" + res.msg);
                        $(document.body).append($form);
                        $form.submit();
                    } else {
                        toastr.error(res.msg);
                    }
                },
                error: function (XMLHttpRequest) {
                    toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
                }
            });
        }
    }
};